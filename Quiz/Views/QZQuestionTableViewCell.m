//
//  QZQuestionTableViewCell.m
//  Quiz
//
//  Created by Camilo Rodríguez Gaviria on 29/04/14.
//  Copyright (c) 2014 Camilo Rodríguez Gaviria. All rights reserved.
//

#import "QZQuestionTableViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import "UIView+AutoLayout.h"

@interface QZQuestionTableViewCell ()

@property (strong, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (assign, nonatomic) BOOL didUpdateConstraints;

@end

@implementation QZQuestionTableViewCell

#pragma mark - Initialization

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.answerLabel = [UILabel newAutoLayoutView];
        self.answerLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        self.answerLabel.numberOfLines = 0;
        self.answerLabel.textAlignment = NSTextAlignmentLeft;
        self.answerLabel.textColor = [UIColor whiteColor];
        self.answerLabel.font = [UIFont systemFontOfSize:14];
        
        [self.contentView addSubview:self.answerLabel];
        
        self.backgroundImageView = [UIImageView newAutoLayoutView];
        
        [self.contentView addSubview:self.backgroundImageView];
    }
    
    return self;
}

#pragma mark - Overridden

- (void)updateConstraints {
    [super updateConstraints];
    
    if (self.didUpdateConstraints) {
        return;
    }
    
    [self.answerLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:0];
    [self.answerLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:30];
    [self.answerLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:50];
    [self.answerLabel autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:1];
    
    self.didUpdateConstraints = YES;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.contentView setNeedsLayout];
    [self.contentView layoutIfNeeded];
    
    self.answerLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.answerLabel.frame);
}

#pragma mark - Setter

- (void)setAnswer:(NSString *)answer {
    _answer = answer;
    self.answerLabel.text = answer;
}

#pragma mark - Public

- (void)configureCellWithAnswer:(NSString *)answer {
    self.answer = answer;
    self.backgroundImageView.image = nil;
    
    if (self.selectedAnswer) {
        [self selectAnswer];
    } else {
        [self deselectAnswer];
    }
}

- (void)selectAnswer {
    self.selectedAnswer = YES;
    self.backgroundImageView.image = [UIImage imageNamed:@"selected"];
}

- (void)deselectAnswer {
    self.selectedAnswer = NO;
    self.backgroundImageView.image = nil;
}

@end
