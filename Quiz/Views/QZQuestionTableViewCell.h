//
//  QZQuestionTableViewCell.h
//  Quiz
//
//  Created by Camilo Rodríguez Gaviria on 29/04/14.
//  Copyright (c) 2014 Camilo Rodríguez Gaviria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QZQuestionTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *answerLabel;
@property (strong, nonatomic) NSString *answer;
@property (assign, nonatomic) BOOL selectedAnswer;

-(void)configureCellWithAnswer:(NSString *)answer;
-(void)selectAnswer;
-(void)deselectAnswer;

@end
