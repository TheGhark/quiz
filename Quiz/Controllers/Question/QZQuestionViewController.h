//
//  QZQuestionViewController.h
//  Quiz
//
//  Created by Camilo Rodríguez Gaviria on 23/04/14.
//  Copyright (c) 2014 Camilo Rodríguez Gaviria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QZQuestion.h"

@interface QZQuestionViewController : UIViewController

- (void)updateQuestion:(QZQuestion *)question;

@end
