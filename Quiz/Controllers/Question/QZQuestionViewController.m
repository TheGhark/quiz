//
//  QZQuestionViewController.m
//  Quiz
//
//  Created by Camilo Rodríguez Gaviria on 23/04/14.
//  Copyright (c) 2014 Camilo Rodríguez Gaviria. All rights reserved.
//

#import "QZQuestionViewController.h"
#import "QZQuestionTableViewCell.h"

NSString *const QZQuestionCellIdentifier= @"QZQuestionCellIdentifier";

@interface QZQuestionViewController () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) QZQuestion *question;
@property (weak, nonatomic) IBOutlet UITableView *answersTableView;
@property (strong, nonatomic) NSMutableArray *dataSource;
@property (strong, nonatomic) QZQuestionTableViewCell *offsetCell;

- (void)deselectCells;

@end

@implementation QZQuestionViewController

#pragma mark - Initialization

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = YES;
}

#pragma mark - Private

- (void)deselectCells {
    for (QZQuestionTableViewCell *cell in [self.answersTableView visibleCells]) {
        [cell deselectAnswer];
    }
}

#pragma mark - Public

- (void)updateQuestion:(QZQuestion *)question {
    self.question = question;
    
    self.dataSource = [NSMutableArray arrayWithObject:question.question];
    [self.dataSource addObjectsFromArray:question.answers];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataSource count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    QZQuestionTableViewCell *cell = self.offsetCell;
    
    if (!cell) {
        cell = [[QZQuestionTableViewCell alloc] init];
    }
    
    if (indexPath.row == 0) {
        cell.answerLabel.font = [UIFont systemFontOfSize:28];
    } else {
        cell.answerLabel.font = [UIFont systemFontOfSize:14];
    }
    
    NSString *answer = [self.dataSource objectAtIndex:indexPath.row];
    cell.answerLabel.text = answer;
    
    [cell setNeedsUpdateConstraints];
    [cell updateConstraintsIfNeeded];
    
    cell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView.bounds), CGRectGetHeight(cell.bounds));
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    
    CGFloat height = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    height += 1;
    
    if (height < 40) {
        height = 40;
    }
    
    self.offsetCell = cell;
    
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    QZQuestionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:QZQuestionCellIdentifier];
    
    if (indexPath.row == 0) {
        cell.answerLabel.font = [UIFont boldSystemFontOfSize:25];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    [cell configureCellWithAnswer:[self.dataSource objectAtIndex:indexPath.row]];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self deselectCells];
    
    if (indexPath.row != 0) {
        QZQuestionTableViewCell *cell = (QZQuestionTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        
        [cell selectAnswer];
    }
}

@end
