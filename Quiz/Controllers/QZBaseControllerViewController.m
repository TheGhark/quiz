//
//  QZBaseControllerViewController.m
//  Quiz
//
//  Created by Camilo Rodríguez Gaviria on 23/04/14.
//  Copyright (c) 2014 Camilo Rodríguez Gaviria. All rights reserved.
//

#import "QZBaseControllerViewController.h"
#import "QZQuestion.h"
#import "QZQuestionViewController.h"

NSString *const QZQuestionSegueID = @"QuestionSegueId";

@interface QZBaseControllerViewController ()

@property (strong, nonatomic) NSMutableArray *questions;

- (void)createQuestions;
- (QZQuestion *)getNewQuestion;

@end

@implementation QZBaseControllerViewController

#pragma mark - Initialization

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self createQuestions];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if ([self.questions count] > 0) {
        [self performSegueWithIdentifier:QZQuestionSegueID sender:nil];
    }
}

#pragma mark - Prepare for Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:QZQuestionSegueID]) {
        QZQuestionViewController *questionVC = segue.destinationViewController;
        [questionVC updateQuestion:[self getNewQuestion]];
    }
}

#pragma mark - Public

- (IBAction)unwindToBase:(UIStoryboardSegue *)segue {
    if ([self.questions count] == 0) {
        [[[UIAlertView alloc] initWithTitle:@"Congratulations!" message:@"There are no more questions to answer. Good job!" delegate:nil cancelButtonTitle:@"DISMISS" otherButtonTitles:nil] show];
    }
}

#pragma mark - Private

- (void)createQuestions {
    self.questions = [NSMutableArray arrayWithCapacity:2];
    
    NSString *questionToAsk = @"Where is Ender's home town?";
    NSArray *answers = @[@"Armenia",
                         @"Salt Lake City, Utah",
                         @"Greensboro, Pennsylvania",
                         @"Rotterdam, Amsterdam"];
    
    QZQuestion *question = [[QZQuestion alloc] initWithQuestion:questionToAsk answers:answers correctAnswer:@"Salt Lake City, Utah"];
    
    [self.questions addObject:question];
    
    questionToAsk = @"Who discover America?\nTake in consideration that discovering as stepping on the ground, not just looking at it. The vikings went there before, if we do not consider stepping on as discovering.";
    answers = @[@"Christopher Columbus, while searching for India",
                @"Amerigo Vespucci, an Italian explorer, financier, navigator and cartographer who first demonstrated that Brazil and the West Indies did not represent Asia's eastern outskirts as initially conjectured from Columbus",
                @"Francis Drake",
                @"Henry Morgan"];
    
    question = [[QZQuestion alloc] initWithQuestion:questionToAsk answers:answers correctAnswer:@"Christopher Columbus"];
    
    [self.questions addObject:question];
}

- (QZQuestion *)getNewQuestion {
    if ([self.questions count] == 0) {
        return nil;
    }
    
    QZQuestion *question = [self.questions objectAtIndex:0];
    [self.questions removeObjectAtIndex:0];
    
    return question;
}

@end
