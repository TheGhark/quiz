//
//  QZBaseControllerViewController.h
//  Quiz
//
//  Created by Camilo Rodríguez Gaviria on 23/04/14.
//  Copyright (c) 2014 Camilo Rodríguez Gaviria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QZBaseControllerViewController : UIViewController

-(IBAction)unwindToBase:(UIStoryboardSegue *)segue;

@end
