//
//  QZQuestion.m
//  Quiz
//
//  Created by Camilo Rodríguez Gaviria on 23/04/14.
//  Copyright (c) 2014 Camilo Rodríguez Gaviria. All rights reserved.
//

#import "QZQuestion.h"

@interface QZQuestion ()

@property (strong, nonatomic) NSString *correctAnswer;

@end

@implementation QZQuestion

#pragma mark - Initialization

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.answers = [NSMutableArray arrayWithCapacity:0];
    }
    
    return self;
}

#pragma mark - Getter

- (BOOL)isCorrect {
    return [self.selectedAnswer isEqualToString:self.correctAnswer];
}

#pragma mark - Public

- (instancetype)initWithQuestion:(NSString *)question answers:(NSArray *)answers correctAnswer:(NSString *)correctAnswer {
    self = [self init];
    
    if (self) {
        self.question = question;
        self.answers = answers;
        self.correctAnswer = correctAnswer;
    }
    
    return self;
}

@end
