//
//  QZQuestion.h
//  Quiz
//
//  Created by Camilo Rodríguez Gaviria on 23/04/14.
//  Copyright (c) 2014 Camilo Rodríguez Gaviria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QZQuestion : NSObject

@property (strong, nonatomic) NSString *question;
@property (strong, nonatomic) NSArray *answers;
@property (strong, nonatomic, readonly) NSString *correctAnswer;
@property (strong, nonatomic) NSString *selectedAnswer;
@property (assign, nonatomic) BOOL isCorrect;

- (instancetype)initWithQuestion:(NSString *)question answers:(NSArray *)answers correctAnswer:(NSString *)correctAnswer;

@end
