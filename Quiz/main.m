//
//  main.m
//  Quiz
//
//  Created by Camilo Rodríguez Gaviria on 23/04/14.
//  Copyright (c) 2014 Camilo Rodríguez Gaviria. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "QZAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([QZAppDelegate class]));
    }
}
